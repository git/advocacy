#!/bin/sh
#
CURDIR=$1

TITLE="Index page for directory ${CURDIR}"

echo "<html>" > index.html
echo "<title>$TITLE</title>" >> index.html
echo "<body>" >> index.html
echo "<h1>$TITLE</h1>" >> index.html
ls \
    | grep -v index.html \
    | sed 's/.*/<a href="&">& <\/a><br\/>/' \
    >> index.html
echo "</body></html>" >> index.html

exit 0
